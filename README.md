# Multipole Kernels
TensorFlow convolutional kernels expanded in multipoles to reduce number of weights in kernels.

Sorry there isn't a lot of content on this page - the documentation in the .py files is excellent though ;).

# TODO
- Write README.md fully
- There is a slight over parameterisation of the kernels because I've not taken out the $\ell<\ell_\textrm{current}$ modes from $\ell_\textrm{current}$. It's not an issue, just not as clean as it could be.
- 
